package com.inakivasquez.rest.utils;

public enum EstadosPedido {
    ACEPTADO,
    COCINADO,
    EN_ENTREGA,
    ENTREGADO,
    VALORADO;
}
