package com.inakivasquez.rest;

import com.inakivasquez.rest.utils.EstadosPedido;
import com.inakivasquez.rest.utils.Utilidades;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.fail;
import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest
{
    //  @Autowired


    @Test
    public void testGetCadena(){
        EmpleadosController empleadosController = new EmpleadosController();
        String correcto = "L.U.Z.D.E.L.S.O.L";
        String origen = "luz del sol";
        assertEquals(correcto, empleadosController.getCadena(origen,"."));

    }

    @Test
    public void testSeparador(){
        try {
            Utilidades.getCadena("Inaki Vasquez", ".");
            fail("Se esperaba BadSeparator");
        } catch (Exception e){
            System.out.println("Nada");
        }
    }
    @Test
    public void testGetAutor(){
        EmpleadosController empleadosController = new EmpleadosController();
        assertEquals("daf",empleadosController.getAutor());
    }

    /*@ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5,6,7,8,9,-3,-16,Integer.MAX_VALUE})
    public void testGetImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }
    */
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ",  "\t", "\n"})
    public void testEstaBlancoCompleto(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void valorarEdoPedido(EstadosPedido ep){
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }

}