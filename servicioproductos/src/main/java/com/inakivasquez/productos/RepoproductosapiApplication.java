package com.inakivasquez.productos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepoproductosapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RepoproductosapiApplication.class, args);
	}

}
